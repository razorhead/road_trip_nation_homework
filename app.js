const express = require('express');
const path = require('path');
const favicon = require('serve-favicon');
const logger = require('morgan');
const cookieParser = require('cookie-parser');
const bodyParser = require('body-parser');

const index = require('./routes/index');
const users = require('./routes/users');

const app = express();
const server = require('http').Server(app);
const io = require('socket.io')(server);
const cache = require('memory-cache');
const feedData = require('./feedData');

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(function(req, res, next){
  res.io = io;
  next();
});
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', index);
app.use('/users', users);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  const err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

// Check for updated feed and send it back to client
io.on('connection', (socket) => {
  const intervalId = setInterval(() => {
    const query = cache.get('query');
    feedData.getFeedData(query)
      .then(xmlContent => {
        if (xmlContent !== cache.get('xmlContent')) {
          feedData.parseXml(xmlContent)
            .then(feed => {
              // render the new content with new feed data
              app.render('feed', { feed }, (err, html) => {
                if (err) {
                  return;
                }
                // Pass the new feed to the client
                io.emit('newFeed', html);
              });
            })
            .catch(error => {
              res.render('feed', { feed: [], error })
            });
        } else {
          console.log('same');
        }
      });
  }, 30000);

  socket.on('disconnect', function() {
    clearInterval(intervalId);
  });
});

module.exports = {app, server};
