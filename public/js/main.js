// receive feed update when available
const socket = io('//localhost:3000');
socket.on('newFeed', function (data) {
  console.log(data);
});
let loader = null;
// Load the feed data once the DOM is ready
window.onload = () => {
  loader = document.getElementById('loader');
  loader.style.display = 'block';
  fetchFeed('realDonaldTrump')
    .then(response => updateUI(response));
  const queryElement = document.getElementById('query');
  // const closeButtons = document.getElementsByClassName('close');
  // const feedElement = document.getElementsByClassName('feed')[0];
  // Add event listener to the query input element
  queryElement.addEventListener('input', fetchNewFeed);
  document.querySelector('body').addEventListener('click', closeFeedCard);
};

function fetchNewFeed(e) {
  const q = e.target.value;
  // display the loader
  loader.style.display = 'block';
  // fetch new data and repopulate fee element
  fetchFeed(q)
    .then(response => updateUI(response))
    .catch(error => {
      console.log(error);
      loader.style.display = 'none';
    });
}

function closeFeedCard(e) {
  if (e.target.tagName.toLowerCase() === 'span') {
    e.target.parentNode.parentNode.removeChild(e.target.parentNode);
  }
}

function fetchFeed(q) {
  return new Promise((resolve, reject) => {
    const xhr = new XMLHttpRequest();
    const url = `http://localhost:3000/api/feed?q=${q}`;
    xhr.open('GET', url, true);
    xhr.onload = () => {
      if(xhr.readyState < 4 || xhr.status !== 200) {
        reject({
          status: this.status,
          statusText: xhr.statusText
        });
      }
      resolve(xhr.response);
    };
    xhr.send();
  });
}

function updateUI(response) {
  setTimeout(() => {
    const feedElement = document.getElementsByClassName('feed')[0];
    const contentElement = document.getElementsByClassName('content')[0];
    const tempDiv = document.createElement('div');
    tempDiv.innerHTML = response;
    loader.style.display = 'none';
    // replace the content
    if (contentElement.contains(feedElement)) {
      contentElement.replaceChild(tempDiv.firstChild, feedElement);
    }
  }, 500);
}
