DROP TABLE IF EXISTS users_classes;
DROP TABLE IF EXISTS classes_lessons;
DROP TABLE IF EXISTS classes;
DROP TABLE IF EXISTS lessons;
DROP TABLE IF EXISTS users;

CREATE TABLE classes (
	id INT UNSIGNED NOT NULL AUTO_INCREMENT,
	name VARCHAR(20), 
	period VARCHAR(20),
	created_at TIMESTAMP NOT NULL DEFAULT NOW(),
	updated_at TIMESTAMP NOT NULL DEFAULT NOW() ON UPDATE NOW(),
	PRIMARY KEY(id)
) ENGINE=INNODB;

INSERT INTO classes (id, name, period) VALUES (1, 'Math', 'Period 2'),(2, 'English', 'Period 4'),(3, 'Science', 'Period 5');

CREATE TABLE lessons (
	id INT UNSIGNED NOT NULL AUTO_INCREMENT,
	name VARCHAR(20), 
	created_at TIMESTAMP NOT NULL DEFAULT NOW(),
	updated_at TIMESTAMP NOT NULL DEFAULT NOW() ON UPDATE NOW(),
	PRIMARY KEY(id)
) ENGINE=INNODB;

INSERT INTO lessons (id, name) VALUES (1, 'Intro'),(2, 'Geometry'),(3, 'Physics'),(4, 'British Literature'),(5, 'Algebra');

CREATE TABLE users (
	id INT UNSIGNED NOT NULL AUTO_INCREMENT,
	first_name VARCHAR(20),
	last_name VARCHAR(20),
	created_at TIMESTAMP NOT NULL DEFAULT NOW(),
	updated_at TIMESTAMP NOT NULL DEFAULT NOW() ON UPDATE NOW(),
	PRIMARY KEY(id)
) ENGINE=INNODB;

INSERT INTO users (id, first_name, last_name) VALUES (1, 'Kevin', 'Parker'),(2, 'Steven', 'Tray'),(3, 'Brittany', 'Chase'),(4, 'Jessica', 'Smith');

CREATE TABLE users_classes (
  	user_id INT UNSIGNED NOT NULL,
	class_id INT UNSIGNED NOT NULL,
  	created_at TIMESTAMP NOT NULL DEFAULT NOW(),
  	updated_at TIMESTAMP NOT NULL DEFAULT NOW() ON UPDATE NOW(),
	PRIMARY KEY (user_id, class_id),
		FOREIGN KEY user_fk (user_id) REFERENCES users (id)
	ON DELETE CASCADE ON UPDATE CASCADE,
		FOREIGN KEY class_fk (class_id) REFERENCES classes (id)
	ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=INNODB;

INSERT INTO users_classes (user_id, class_id) VALUES (1,2),(1,3),(2,1),(2,3),(3,1),(3,2),(3,3),(4,1),(4,2);

CREATE TABLE classes_lessons (
	class_id INT UNSIGNED NOT NULL,
	lesson_id INT UNSIGNED NOT NULL,
	created_at TIMESTAMP NOT NULL DEFAULT NOW(),
	updated_at TIMESTAMP NOT NULL DEFAULT NOW() ON UPDATE NOW(),
	PRIMARY KEY (class_id, lesson_id),
	FOREIGN KEY class_fk (class_id) REFERENCES classes (id)
		ON DELETE CASCADE ON UPDATE CASCADE,
	FOREIGN KEY lesson_fk (lesson_id) REFERENCES lessons (id)
		ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=INNODB;

INSERT INTO classes_lessons (class_id, lesson_id) VALUES (1,1),(1,2),(1,5),(2,1),(2,4),(3,1),(3,5),(3,3);

-- Show all of the lessons User 1 is taking
SELECT DISTINCT student_name, lesson_name
FROM
	(SELECT CONCAT(u.first_name, ' ', u.last_name) AS student_name, uc.`class_id` AS classId FROM users_classes uc
	INNER JOIN users u on u.id = uc.user_id WHERE u.id = 1) AS a
JOIN
	(SELECT c.name, c.id AS classId, l.name AS lesson_name FROM classes_lessons cl
	INNER JOIN classes c ON c.id = cl.class_id
	INNER JOIN lessons l ON l.id = cl.lesson_id) AS b
WHERE b.classId = a.classId

-- 
