const request = require('request');
const xml2js = require('xml2js');
const cache = require('memory-cache');

/**
 * Get rss feed from queryfeed.net
 * @property {string} query
 * @returns {Promise}
 */
function getFeedData(query) {
  return new Promise((resolve, reject) => {
    request(`https://queryfeed.net/twitter?q=${query}`, (error, response, body) => {
      if (error) {
        return reject('failed to get feed from queryfeed');
      }
      cache.put('xmlContent', body);
      return resolve(body);
    });
  });
}

/**
 * Parse the xml feed returned by queryfeed.net
 * @property {string} xmlContent
 * @returns {Promise}
 */
function parseXml (xmlContent) {
  return new Promise((resolve, reject) => {
    const parseString = xml2js.parseString;
    parseString(xmlContent, (err, result) => {
      if (err) {
        return resolve([]);
      }
      if (result.rss) {
        return resolve(result.rss.channel[0].item);
      }
      return resolve([]);

    });
  });
}

module.exports = {getFeedData, parseXml};
