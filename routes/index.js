const express = require('express');
const router = express.Router();
const cache = require('memory-cache');
const feedData = require('../feedData');

// Options for toLocaleString which is used on the front-end
const dateFormatOptions = { weekday: 'short', year: 'numeric', month: 'long', day: 'numeric', hour: '2-digit', minute: '2-digit'};

/* GET home page. */
router.get('/', (req, res, next) => {
  res.render('index', {});
});

router.get('/api/feed', (req, res, next) => {
  const query = req.query.q;
  cache.put('query', query);
  feedData.getFeedData(query)
    .then(xmlContent => feedData.parseXml(xmlContent))
    .then(feed => res.render('feed', { feed, dateFormatOptions }))
    .catch(error => res.render('feed', { feed: [], error }));
});

module.exports = router;
